# Wolfy

## Résumé

Application permettant de tester différentes fonctionnalités :

- Les Flux RSS pour le site  `Le Monde`
- L'API de Youtube avec la chaîne d'un développeur mobile iOS `Lets Build That App`
- Firebase avec la création d'un `Forum` comprenant :
	- la création de compte
	- l'authentification
	- un chat afin de poser et répondre à des questions poser sur le forum
	- Recherche de question