//
//  NavigationController.swift
//  Wolfy
//
//  Created by Nessim Halfon on 19/03/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit

class NavigationController: UINavigationController {

    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationBar.barTintColor = #colorLiteral(red: 0.2099020481, green: 0.2616115808, blue: 0.3281416297, alpha: 1)
        navigationBar.tintColor = #colorLiteral(red: 0.841610074, green: 0.8896840215, blue: 0.9313734174, alpha: 1)
        navigationBar.titleTextAttributes = [
            .foregroundColor: #colorLiteral(red: 0.841610074, green: 0.8896840215, blue: 0.9313734174, alpha: 1),
            .font: UIFont.italicSystemFont(ofSize: 20)
        ]
    }
}
