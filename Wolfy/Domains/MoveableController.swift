//
//  MoveableController.swift
//  Wolfy
//
//  Created by Nessim Halfon on 25/03/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit

class MoveableController: UIViewController {

    //MARK:- Properties
    var height: CGFloat = 0
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(showKeyboard), name: UIWindow.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(hideKeyboard), name: UIWindow.keyboardWillHideNotification, object: nil)
        
    }
    
    //MARK:- Func
    
    func addTapGesture() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(tap))
        view.addGestureRecognizer(gesture)
    }
    
    @objc func tap() {
        view.endEditing(true)
    }
    
    @objc func showKeyboard(notification: Notification) {
        if let keyHeight = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
             height = keyHeight
        }
       
    }
    
    @objc func hideKeyboard(notification: Notification) {
        
    }
    
    func animation(_ constante: CGFloat, _ constraint: NSLayoutConstraint) {
        UIView.animate(withDuration: 0.5) {
            constraint.constant = constante
        }
    }
    
    func checkHeight(_ view: UIView, constraint:  NSLayoutConstraint) {
        let bottom = view.frame.maxY
        let screenHeight =  UIScreen.main.bounds.height
        let remain = screenHeight - bottom - 20
        if height > remain {
            let constant = remain - height
            animation(constant, constraint)
        }
    }
    
    //MARK:- IBAction
    

}
