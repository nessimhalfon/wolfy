//
//  AnswerController.swift
//  Wolfy
//
//  Created by Nessim Halfon on 26/03/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit

class AnswerController: MoveableController {

    //MARK:- IBOutlet
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var profilImageView: RoundImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var questionLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var chatTextView: UITextView!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var libraryButton: UIButton!
    @IBOutlet weak var leadingLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomLayoutConstraint: NSLayoutConstraint!
    
    
    //MARK:- Properties
    let service = FirebaseService()
    var question: Question?
    var answers: [Answer] = []
    let imagePicker =  UIImagePickerController()
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setupTopView()
        setupBottomView()
        fetchAnswer()
    }
    
    //MARK:- Func
    
    private func setupTopView() {
        setupQuestion()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tap))
        topView.addGestureRecognizer(tapGesture)
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
    }
    
    private func setupQuestion() {
        guard let question = question else { return }
        questionLabel.text = question.question
        dateLabel.text = question.date.timeSince()
        service.fetchUser(question.userId) { user in
            DispatchQueue.main.async {
                guard let user = user else { return }
                self.profilImageView.download(string: user.imageUrl)
                self.usernameLabel.text = user.username
            }
        }
    }
    
    private func setupBottomView() {
        chatTextView.layer.cornerRadius = 15
        chatTextView.delegate = self
    }
    
    private func fetchAnswer() {
        guard let question = question else { return }
        service.fetchAnswer(ref: question.ref) { answer in
            DispatchQueue.main.async {
                self.answers.append(answer)
                self.tableView.reloadData()
            }
        }
    }
    
    
    //MARK:- Func Keyboard
    override func showKeyboard(notification: Notification) {
        super.showKeyboard(notification: notification)
        
        let keyWindow = UIApplication.shared.connectedScenes
        .filter({$0.activationState == .foregroundActive})
        .map({$0 as? UIWindowScene})
        .compactMap({$0})
        .first?.windows
        .filter({$0.isKeyWindow}).first
        
        let safeArea = keyWindow?.safeAreaInsets.bottom ?? 0
        
        let totalHeight = height - safeArea
        animation(totalHeight, bottomLayoutConstraint)
        
    }
    
    override func hideKeyboard(notification: Notification) {
        super.hideKeyboard(notification: notification)
        animation(0, bottomLayoutConstraint)
    }
    
    
    //MARK:- IBAction
    
    @IBAction func cameraButton(_ sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.sourceType = .camera
            present(imagePicker, animated: true)
        }
    }
    
    @IBAction func libraryButton(_ sender: UIButton) {
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true)
    }
    
    @IBAction func validateButton(_ sender: UIButton) {
        guard let question = question,
            let answer = chatTextView.text, answer.count > 0 else { return }
        
        service.uploadAnswer(ref: question.ref, answer: answer, image: nil, height: nil)
        chatTextView.text = nil
        animatIn(false)
        animation(40, heightLayoutConstraint)
        view.endEditing(true)
    }
    
}

extension AnswerController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return answers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: AnswerCell.identifier, for: indexPath) as? AnswerCell else { return UITableViewCell() }
        
        let answer = answers[indexPath.row]
       
        cell.setup(answer, isColor: indexPath.row % 2 != 0)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let answer = answers[indexPath.row]
        var base: CGFloat = 100
        if answer.answer != nil {
             return UITableView.automaticDimension
        }
        if answer.imageHeight != nil {
            let heightImage = (tableView.frame.width - 16) * answer.imageHeight!
            base += heightImage
        }
        
        return base
    }
}

//MARK:-
extension AnswerController: UITextViewDelegate {
    
    func animatIn(_ bool: Bool) {
        libraryButton.isHidden = bool
        cameraButton.isHidden = bool
        let constant: CGFloat = bool ? 8 : 90
        animation(constant, leadingLayoutConstraint)
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.count == 0 {
            animation(40, heightLayoutConstraint)
            animatIn(false)
        } else {
            guard let text = textView.text else { return }
            let textHeight = text.height(textView.frame.height, font: .systemFont(ofSize: 15)) + 25
            animation(textHeight, heightLayoutConstraint)
            animatIn(true)
        }
    }
    
   
    
    
}

//MARK:-
extension AnswerController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
           
        if let image = info[.originalImage] as? UIImage {
            let imageSize = image.size
            let heightRatio = imageSize.height / imageSize.width
            if let data = image.jpegData(compressionQuality: 0.4) {
                service.uploadImageAnswer(data) { urlString in
                    if urlString != nil, self.question != nil {
                        self.service.uploadAnswer(ref: self.question!.ref, answer: nil, image: urlString!, height: heightRatio)
                    }
                }
            }
        }
           
        dismiss(animated: true)
    }
       
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true)
    }

}
