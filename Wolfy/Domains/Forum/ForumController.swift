//
//  ForumController.swift
//  Wolfy
//
//  Created by Nessim Halfon on 19/03/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit

class ForumController: UIViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- Properties
    let service = FirebaseService()
    var questions: [Question] = []
    var questionsTriees: [Question] = []
    var tri = false
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        fetchQuestion()

    }
    
    
    //MARK:- Func
    
    private func fetchQuestion() {
        service.fetchQuestion { question in
            DispatchQueue.main.async {
                self.questions.append(question)
                self.tableView.reloadData()
            }
        }
    }
    
    func getQuestion(indexPath: IndexPath) -> Question {
        switch tri {
            case true:  return questionsTriees[indexPath.row]
            case false:   return questions[indexPath.row]
        }
    }
       
    func askQuestion() {
        let alert = UIAlertController(title: "Posez votre question", message: nil, preferredStyle: .alert)
        alert.addTextField { textField in
            textField.placeholder = "Posez votre question"
        }
       
        let okAction = UIAlertAction(title: "Ok", style: .default) { action in
            if let textFields = alert.textFields {
                if textFields.count > 0 {
                    let textField = textFields[0]
                    if let text = textField.text, text.count > 0 {
                        self.service.uploadQuestion(text)
                    }
                }
            }
        }
        let cancelAction = UIAlertAction(title: "Annuler", style: .cancel)
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        present(alert, animated: true)
    }
    
    
    
    //MARK:- IBAction

    @IBAction func addQuestions(_ sender: UIBarButtonItem) {
        askQuestion()
    }
    
}

//MARK:-
extension ForumController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tri ? questionsTriees.count : questions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: QuestionCell.identifier, for: indexPath) as? QuestionCell else { return UITableViewCell() }
        
        let question = questions[indexPath.row]
        
        cell.setup(date: question.date, question: question.question)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let question = getQuestion(indexPath: indexPath)
        performSegue(withIdentifier: "showAnswer", sender: question)
    }
    
   
}

//MARK:-
extension ForumController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if let text = searchBar.text, text.count > 0 {
            tri = true
            questionsTriees = questions.filter {
                $0.question.lowercased().contains(text.lowercased())
            }
        } else {
            tri = false
            
        }
        tableView.reloadData()
        
    }
}

extension ForumController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showAnswer",
            let controller = segue.destination as? AnswerController {
            let question = sender as? Question
            controller.question = question
        }
    }
}
