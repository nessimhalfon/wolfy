//
//  AnswerCell.swift
//  Wolfy
//
//  Created by Nessim Halfon on 26/03/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit

class AnswerCell: UITableViewCell {

    //MARK:- Static Properties
    static let identifier = "AnswerCell"
    
    //MARK:- Properties
    var answer: Answer!
    let service = FirebaseService()
    
    //MARK:- IBOutlet
    @IBOutlet weak var profilImageView: RoundImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var answerLabel: UILabel!
    @IBOutlet weak var answerImageView: UIImageView!
    
    //MARK:- Func

    func setup(_ answer: Answer, isColor: Bool) {
        answerLabel.isHidden = true
        answerImageView.isHidden = true
        self.answer = answer
        
        usernameLabel.textColor = isColor ? #colorLiteral(red: 0.841610074, green: 0.8896840215, blue: 0.9313734174, alpha: 1) : #colorLiteral(red: 0.2099020481, green: 0.2616115808, blue: 0.3281416297, alpha: 1)
        answerLabel.textColor = isColor ? #colorLiteral(red: 0.841610074, green: 0.8896840215, blue: 0.9313734174, alpha: 1) : #colorLiteral(red: 0.2099020481, green: 0.2616115808, blue: 0.3281416297, alpha: 1)
        contentView.backgroundColor = isColor ? #colorLiteral(red: 0.2099020481, green: 0.2616115808, blue: 0.3281416297, alpha: 1) : #colorLiteral(red: 0.841610074, green: 0.8896840215, blue: 0.9313734174, alpha: 1)
        
        dateLabel.text = self.answer.date?.timeSince()
        service.fetchUser(self.answer.userId) { user in
            DispatchQueue.main.async {
                guard let user = user else { return }
                self.profilImageView.download(string: user.imageUrl)
                self.usernameLabel.text = user.username
            }
        }
        
        if let text = self.answer.answer {
            answerLabel.isHidden = false
            answerLabel.text = text
        }
        if let image = self.answer.imageUrl {
            answerImageView.isHidden = false
            answerImageView.download(string: image)
        }
    }
}
