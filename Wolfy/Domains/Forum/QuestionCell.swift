//
//  QuestionCell.swift
//  Wolfy
//
//  Created by Nessim Halfon on 26/03/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit

class QuestionCell: UITableViewCell {

    //MARK:- Static Properties
    static let identifier = "QuestionCell"
    
    //MARK:- IBOutlet
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var questionLabel: UILabel!
    
    //MARK:- Func
    
    func setup(date: String, question: String) {
        dateLabel.text = date.timeSince()
        questionLabel.text = question
    }

}
