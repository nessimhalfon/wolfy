//
//  MenuController.swift
//  Wolfy
//
//  Created by Nessim Halfon on 19/03/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit
import CircleMenu

class MenuController: UIViewController {
    
    //MARK:- IBOutlet
    @IBOutlet weak var circleButton: CircleMenu!
    @IBOutlet weak var profileImageView: RoundImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var logButton: UIButton!
    
    //MARK:- Properties
    
    let items: [(icon: String, color: UIColor)] = [ ("actu", #colorLiteral(red: 0.841610074, green: 0.8896840215, blue: 0.9313734174, alpha: 1)),  ("forum", #colorLiteral(red: 0.841610074, green: 0.8896840215, blue: 0.9313734174, alpha: 1)), ("video",#colorLiteral(red: 0.841610074, green: 0.8896840215, blue: 0.9313734174, alpha: 1)) ]
    let service = FirebaseService()
    var user: User?


    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setupMenu()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let id = service.login() {
            service.fetchUser(id) { user in
                DispatchQueue.main.async {
                    self.user = user
                    if self.user != nil {
                        self.logButton.setTitle("Profil", for: .normal)
                        self.usernameLabel.text = user?.username
                        self.profileImageView.download(string: user!.imageUrl)
                    }
                }
            }
        } else {
            // Logout
            self.user = nil
            logButton.setTitle("Se connecter", for: .normal)
            usernameLabel.text = ""
        }
    }
    
    //MARK:- Setup Func
    private func setupMenu() {
        circleButton.delegate = self
        circleButton.layer.cornerRadius = circleButton.frame.size.width / 2
        circleButton.layer.borderWidth = 2
        circleButton.layer.borderColor = #colorLiteral(red: 0.2099020481, green: 0.2616115808, blue: 0.3281416297, alpha: 1)
    }
    
    //MARK:- IBAction
    
    @IBAction func loginButton(_ sender: UIButton) {
        if user != nil {
            performSegue(withIdentifier: "showProfil", sender: user!)
        } else {
            performSegue(withIdentifier: "logIn", sender: nil)
        }
    }
}

//MARK:-
extension MenuController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showProfil",
            let nav = segue.destination as? NavigationController,
            let first = nav.topViewController as? ProfilController {
                let user = sender as? User
                first.user = user
        }
    }
}

//MARK:-
extension MenuController: CircleMenuDelegate {
    
    func circleMenu(_ circleMenu: CircleMenu, willDisplay button: UIButton, atIndex: Int) {
        
        button.layer.borderWidth = 2
        button.layer.borderColor = #colorLiteral(red: 0.2099020481, green: 0.2616115808, blue: 0.3281416297, alpha: 1)

        button.backgroundColor = items[atIndex].color
        button.setImage(UIImage(named: items[atIndex].icon), for: .normal)

        // set highlited image
        let highlightedImage = UIImage(named: items[atIndex].icon)?.withRenderingMode(.alwaysTemplate)
        button.setImage(highlightedImage, for: .highlighted)
        button.tintColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        
    }

    // call after animation
    func circleMenu(_ circleMenu: CircleMenu, buttonDidSelected button: UIButton, atIndex: Int) {
        
        switch atIndex {
            case 0:
                let actu = presentViewController(withName: "Actu", withIdentifier: "ActuController")
                navigationController?.pushViewController(actu, animated: true)
            case 1:
                if service.login() == nil {
                    displayAlert(title: "Erreur", message: "Vous devez être connecté pour accéder au forum.")
                } else {
                    let forum = presentViewController(withName: "Forum", withIdentifier: "ForumController")
                    navigationController?.pushViewController(forum, animated: true)
                }
            case 2:
                let video = presentViewController(withName: "Video", withIdentifier: "VideoController")
                navigationController?.pushViewController(video, animated: true)
            default: break
        }
    }
}
