//
//  ProfilController.swift
//  Wolfy
//
//  Created by Nessim Halfon on 25/03/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit

class ProfilController: MoveableController {

    //MARK:- IBOutlet
    @IBOutlet weak var logView: CustomView!
    @IBOutlet weak var profileImageView: RoundImageView!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var validerButton: UIButton!
    @IBOutlet weak var centerViewLayoutConstraint: NSLayoutConstraint!
    
    
    //MARK:- Properties
    let service = FirebaseService()
    var user: User?
    var canAdd = false
    let imagePicker =  UIImagePickerController()
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUser()
        setupProfilImage()
        addTapGesture()
    }
    
    //MARK:- Func
    
    private func setupUser() {
        if let user = user {
            profileImageView.download(string: user.imageUrl)
            usernameTextField.placeholder = user.username
        }
    }
    
    private func setupProfilImage() {
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
    }
    
    
    //MARK:- Objc Func Keyboard
    override func showKeyboard(notification: Notification) {
        super.showKeyboard(notification: notification)
        checkHeight(logView, constraint: centerViewLayoutConstraint)
    }
    
    override func hideKeyboard(notification: Notification) {
        super.hideKeyboard(notification: notification)
        animation(0, centerViewLayoutConstraint)
    }
    
    //MARK:- IBAction
    @IBAction func backButton(_ sender: UIBarButtonItem) {
        dismiss(animated: true)
    }
    
    @IBAction func logoutButton(_ sender: UIButton) {
        service.signOut()
        dismiss(animated: true)
    }
    
    @IBAction func cameraButton(_ sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.sourceType = .camera
            present(imagePicker, animated: true)
        }
    }
    
    @IBAction func libraryButton(_ sender: UIButton) {
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true)
    }
    
    
    @IBAction func valideButton(_ sender: UIButton) {
        view.endEditing(true)
        
        guard let username = usernameTextField.text else { return }
        
        if canAdd {
            if let user = user {
                service.updateUser(user.id, dict: ["username": username])
                dismiss(animated: true)
            }
        }
    }
    
    @IBAction func usernameChanged(_ sender: UITextField) {
        service.usernameExist(sender.text) { (bool, string) in
            DispatchQueue.main.async {
                self.errorLabel.text = string
                self.canAdd = bool
            }
        }
    }
}

//MARK:-
extension ProfilController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let original = info[.originalImage] as? UIImage {
            self.profileImageView.image = original
            
            if let data = original.jpegData(compressionQuality: 0.2) {
                service.uploadProfilPicture(data)
            }
        }
        
        dismiss(animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true)
    }
    
}
