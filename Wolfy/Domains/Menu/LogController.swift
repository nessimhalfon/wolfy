//
//  LogController.swift
//  Wolfy
//
//  Created by Nessim Halfon on 25/03/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit

class LogController: MoveableController {

    //MARK:- IBOutlet
    @IBOutlet weak var logView: CustomView!
    @IBOutlet weak var centerViewLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var usernameErrorLabel: UILabel!
    @IBOutlet weak var connectButton: UIButton!
    
    //MARK:- Properties
    let service = FirebaseService()
    var canAdd = false
    
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
        addTapGesture()
    }
    
    
    //MARK:- Func
    private func setup() {
        switch segmentedControl.selectedSegmentIndex {
            case 0:
                usernameTextField.isHidden = true
                connectButton.setTitle("Se connecter", for: .normal)
            case 1:
                usernameTextField.isHidden = false
                connectButton.setTitle("Créer un compte", for: .normal)
            default:break
        }
    }
    
    private func logCompletion(_ bool: Bool?, _ error: Error?) {
        if error != nil {
            self.displayAlert(title: "Erreur", message: error?.localizedDescription)
        }
        if bool != nil, bool == true {
            self.dismiss(animated: true)
        }
    }
    
    private func login() {
        guard let email = emailTextField.text, email.count > 0,
            let password = passwordTextField.text, password.count > 0 else {
            displayAlert(title: "Erreur", message: "Email ou Mot de passe vide.")
            return
        }
        
        switch segmentedControl.selectedSegmentIndex {
            case 0:
                service.signIn(email, password: password, result: logCompletion)
            case 1:
                guard let username = usernameTextField.text, username.count > 0 else {
                    displayAlert(title: "Erreur", message: "Username vide.")
                    return
                }
                if canAdd {
                    service.create(email, password: password, username: username, result: logCompletion)
                }
             
            default: break
        }
    }
    
    //MARK:- Func Keyboard
    override func showKeyboard(notification: Notification) {
        super.showKeyboard(notification: notification)
        checkHeight(logView, constraint: centerViewLayoutConstraint)
    }
    
    override func hideKeyboard(notification: Notification) {
        super.hideKeyboard(notification: notification)
        animation(0, centerViewLayoutConstraint)
    }
    
    //MARK:- IBAction
    
    @IBAction func segmentedChanged(_ sender: UISegmentedControl) {
        setup()
    }
    
    @IBAction func usernameChanged(_ sender: UITextField) {
        service.usernameExist(sender.text) { (bool, string) in
            DispatchQueue.main.async {
                self.canAdd = bool
                self.usernameErrorLabel.text = string
            }
        }
    }
    
    @IBAction func loginButton(_ sender: UIButton) {
        login()
    }
    
    @IBAction func backButton(_ sender: UIBarButtonItem) {
        dismiss(animated: true)
    }
    
    

}
