//
//  VideoController.swift
//  Wolfy
//
//  Created by Nessim Halfon on 19/03/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit

class VideoController: UIViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- Properties
    let service = YoutubeAPIService()
    var videos: [Video] = []
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetch()
    }
    
    //MARK:- Func
    
    private func fetch() {
        service.getVideos { videos in
            DispatchQueue.main.async {
                self.videos = videos
                self.tableView.reloadData()
            }
        }
    }
}

//MARK:-
extension VideoController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return videos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "VideoCell", for: indexPath) as? VideoCell
            else { return UITableViewCell() }
        
        let video = videos[indexPath.row].snippet
        title = video.channelTitle
        cell.setup(title: video.title, imageURL: video.thumbnails.high.url)
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let baseUrlVideo = "https://www.youtube.com/watch?v="
        let urlString = baseUrlVideo + videos[indexPath.row].id.videoId
        performSegue(withIdentifier: "detailVideo", sender: urlString)
    }
}

//MARK:-
extension VideoController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailVideo",
            let controller = segue.destination as? WebController {
                let urlString = sender as? String
                controller.urlString = urlString
        }
    }
}
