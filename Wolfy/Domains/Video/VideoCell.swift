//
//  VideoCell.swift
//  Wolfy
//
//  Created by Nessim Halfon on 25/03/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit
import SDWebImage

class VideoCell: UITableViewCell {

    //MARK:- Static Properties
    static let identifier = "VideoCell"
    
    //MARK:- IBOutlet
    @IBOutlet weak var videoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    
    //MARK:- Func
    
    func setup(title: String, imageURL: String) {
        titleLabel.text = title
        videoImageView.download(string: imageURL)
    }

}
