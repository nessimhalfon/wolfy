//
//  ArticleCell.swift
//  Wolfy
//
//  Created by Nessim Halfon on 19/03/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit
import SDWebImage

class ArticleCell: UITableViewCell {

    //MARK:- Static Properties
    static let identifier = "ArticleCell"
    
    //MARK:- IBOutlet
    @IBOutlet weak var articleImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    //MARK:- Func
    func setup(_ title: String, date: String, imageUrl: String) {
        titleLabel.text = title
        dateLabel.text = date.timeSince()
        articleImageView.download(string: imageUrl)
    }

}
