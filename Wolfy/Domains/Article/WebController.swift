//
//  WebController.swift
//  Wolfy
//
//  Created by Nessim Halfon on 19/03/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit
import WebKit

class WebController: UIViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var webView: WKWebView!
    
    //MARK:- Properties
    var urlString: String?
    var loadingImageView: LoadingImageView?
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setupWebView()
    }
    
    //MARK:- Setup Func
    
    private func setupWebView() {
        guard let link = urlString,
            let url = URL(string: link) else { return }
        let urlRequest = URLRequest(url: url)
        webView.navigationDelegate = self
        createLoadingAnimation()
        
        webView.load(urlRequest)
    }
    
    private func createLoadingAnimation() {
        let frame = CGRect(x: view.frame.width / 2 - 75, y: view.frame.height / 2 - 75, width: 150, height: 150)
        loadingImageView = LoadingImageView(frame: frame)
        loadingImageView?.start()
        if loadingImageView != nil {
            view.addSubview(loadingImageView!)
        }
    }
}

//MARK:-
extension WebController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        loadingImageView?.stop()
        loadingImageView?.removeFromSuperview()
        loadingImageView = nil
    }
}
