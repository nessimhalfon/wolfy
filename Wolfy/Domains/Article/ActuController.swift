//
//  ActuController.swift
//  Wolfy
//
//  Created by Nessim Halfon on 19/03/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit

class ActuController: UIViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- Properties
    var articles: [Article] = []
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setupTableView()
        parse()
    }
    
    //MARK:- Setup View
    private func setupTableView() {
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.tintColor = #colorLiteral(red: 0.2099020481, green: 0.2616115808, blue: 0.3281416297, alpha: 1)
        tableView.refreshControl?.addTarget(self, action: #selector(parse), for: .valueChanged)
    }
    
    //MARK:- Func
    
    @objc private func parse() {
        RSSParser().parse("https://www.lemonde.fr/rss/une.xml") { articles in
            DispatchQueue.main.async {
                self.articles = articles
                self.tableView.reloadData()
                if self.tableView.refreshControl?.isRefreshing ?? false {
                    self.tableView.refreshControl?.endRefreshing()
                }
            }
        }
    }
    
}

//MARK:-
extension ActuController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ArticleCell.identifier, for: indexPath) as? ArticleCell else { return UITableViewCell() }
        
        let article = articles[indexPath.row]
        
        cell.setup(article.title, date: article.pubDate, imageUrl: article.imageUrl)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let urlString = articles[indexPath.row].link
        performSegue(withIdentifier: "detailArticle", sender: urlString)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 280
    }
}

//MARK:-
extension ActuController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailArticle",
            let controller = segue.destination as? WebController {
                let urlString = sender as? String
                controller.urlString = urlString
        }
    }
}
