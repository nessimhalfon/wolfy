//
//  UIViewController.swift
//  Wolfy
//
//  Created by Nessim Halfon on 19/03/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit

//MARK:- Present

extension UIViewController {
    
    /// Permet d'afficher une popup avec un message.
    func displayAlert(title: String?, message: String?, completion: (() -> Void)? = nil, handler: ((UIAlertAction) -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: handler))
        present(alert, animated: true, completion: completion)
    }
    
    /// Permet de préparer le present avec un Storyboard.
    /// - parameter name : nom du storyboard.
    /// - parameter identifier : nom du viewController.
    /// - returns : Le view controller à present/push.
    func presentViewController(withName name: String, withIdentifier identifier: String) -> UIViewController {
        return UIStoryboard(name: name, bundle: nil).instantiateViewController(withIdentifier: identifier)
    }
}
