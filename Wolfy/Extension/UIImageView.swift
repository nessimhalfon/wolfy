//
//  UIImageView.swift
//  Wolfy
//
//  Created by Nessim Halfon on 26/03/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit
import SDWebImage

extension UIImageView {
    
    func download(string: String) {
        sd_setImage(with: URL(string: string), placeholderImage: UIImage(named: "logo"), options: .highPriority, completed: nil)
    }
    
}
