//
//  Date.swift
//  Wolfy
//
//  Created by Nessim Halfon on 19/03/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit

//MARK:-
extension Formatter {
    
    static let iso8601: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_EN")
        formatter.dateFormat = "E, dd MMM yyyy HH:mm:ss Z"
        return formatter
    }()

}

//MARK:-
extension Date {
    
    func toString() -> String {
        return Formatter.iso8601.string(from: self)
    }
}


//MARK:-
extension String {
    var iso8601: Date {
        return Formatter.iso8601.date(from: self) ?? Date()
    }

    func timeSince() -> String {
        let date = iso8601
        let calendar = Calendar.current
        let components = calendar.dateComponents([.month, .day, .hour, .minute], from: date, to: Date())
        let month = components.month ?? 0
        let day = components.day ?? 0
        let hour = components.hour ?? 0
        let minute = components.minute ?? 0
        
        var base = "Il y a "
        
        if month > 0 {
            return base + String(month) + " mois"
        }
        if day > 0 {
            base = base + String(day) + " jour"
            if day > 1 {
                base += "s"
            }
            return base
        }
        if hour > 0 {
            base = base + String(hour) + " heure"
            if hour > 1 {
                base += "s"
            }
            return base
        }
        if minute > 0 {
            base = base + String(minute) + " minute"
            if minute > 1 {
                base += "s"
            }
            return base
        }
        
        return "À l'instant"
        
    }
    
    
    func height(_ width: CGFloat, font: UIFont) -> CGFloat {
        let size = CGSize(width: width, height: .greatestFiniteMagnitude)
        let option = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let boundingBox = NSString(string: self).boundingRect(with: size, options: option, attributes: [.font: font], context: nil)
        return boundingBox.height
   }
    
}

