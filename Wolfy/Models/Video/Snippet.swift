//
//  Snippet.swift
//  Wolfy
//
//  Created by Nessim Halfon on 25/03/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import Foundation

struct Snippet: Decodable {
    let title: String
    let description: String
    let thumbnails: Thumbnails
    let channelTitle: String
}
