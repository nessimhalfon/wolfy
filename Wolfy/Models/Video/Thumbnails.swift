//
//  Thumbnails.swift
//  Wolfy
//
//  Created by Nessim Halfon on 25/03/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import Foundation

struct Thumbnails: Decodable {
    let medium: Image
    let high: Image
}
