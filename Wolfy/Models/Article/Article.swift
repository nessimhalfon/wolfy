//
//  Article.swift
//  Wolfy
//
//  Created by Nessim Halfon on 19/03/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import Foundation

struct Article {
    var title: String
    var link: String
    var pubDate: String
    var imageUrl: String
}
