//
//  Question.swift
//  Wolfy
//
//  Created by Nessim Halfon on 26/03/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import Foundation
import FirebaseDatabase


struct Question {
    
    private let _ref: DatabaseReference
    private let _id: String
    private let _date: String
    private let _userId: String
    private let _question: String
    
    var ref: DatabaseReference {
        return _ref
    }
    
    var id: String {
        return _id
    }
    
    var date: String {
        return _date
    }
    
    var userId: String {
        return _userId
    }
    
    var question: String {
        return _question
    }
    
    //MARK:- Init
    
    init(ref: DatabaseReference, id: String, date: String, userId: String, question: String) {
        self._ref = ref
        self._id = id
        self._date = date
        self._userId = userId
        self._question = question
    }
}
