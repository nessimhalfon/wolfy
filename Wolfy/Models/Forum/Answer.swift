//
//  Answer.swift
//  Wolfy
//
//  Created by Nessim Halfon on 26/03/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import FirebaseDatabase

struct Answer {
    
    private let _ref: DatabaseReference
    private let _id: String
    private let _userId: String
    private var _date: String?
    private var _answer: String?
    private var _imageUrl: String?
    private var _imageHeight: CGFloat?
    
    var ref: DatabaseReference {
        return _ref
    }
    
    var id: String {
        return _id
    }
    
    var userId: String {
        return _userId
    }
    
    var date: String? {
        return _date ?? Date().toString()
    }
    
    var answer: String? {
        return _answer
    }
    
    var imageUrl: String? {
        return _imageUrl
    }
    
    var imageHeight: CGFloat? {
        return _imageHeight
    }
    
    
    //MARK:- Init
    
    init(userId: String, snapshot: DataSnapshot) {
        self._ref = snapshot.ref
        self._id = snapshot.key
        self._userId = userId
        
        if let dict = snapshot.value as? [String:Any] {
            self._answer = dict["answer"] as? String
            self._date = dict["date"] as? String
            self._imageUrl = dict["imageUrl"] as? String
            self._imageHeight = dict["imageHeight"] as? CGFloat
        }
    }
}
