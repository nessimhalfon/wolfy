//
//  User.swift
//  Wolfy
//
//  Created by Nessim Halfon on 25/03/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import Foundation
import FirebaseDatabase

class User {
    
    private let _ref: DatabaseReference
    private let _id: String
    private var _username: String?
    private var _imageUrl: String?
    
    //MARK:- Getter
    
    var id: String {
        return _id
    }
    
    var username: String {
        return _username ?? "Anonyme"
    }
    
    var imageUrl: String {
        return _imageUrl ?? ""
    }
    
    var ref: DatabaseReference {
        return _ref
    }
    
    //MARK:- Init
    
    init(snapshot: DataSnapshot) {
        self._ref = snapshot.ref
        self._id = snapshot.key
        guard let dict = snapshot.value as? [String:String] else { return }
        self._username = dict["username"]
        self._imageUrl = dict["imageUrl"]
    }
}
