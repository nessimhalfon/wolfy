//
//  LoadingImageView.swift
//  Wolfy
//
//  Created by Nessim Halfon on 19/03/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit

class LoadingImageView: UIImageView {

    //MARK:- Properties
    var timer = Timer()
    
    //MARK:- Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    //MARK:- Setup Func
    
    func setup() {
        image = UIImage(named: "loading")
        contentMode = .scaleAspectFit
    }
    
    //MARK:- Func
    
    func start() {
        timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true) { timer in
            UIView.transition(with: self, duration: 0.5, options: .transitionFlipFromBottom, animations: nil, completion: nil)
        }
    }
    
    func stop() {
        timer.invalidate()
    }
    

}
