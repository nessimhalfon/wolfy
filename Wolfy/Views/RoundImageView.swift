//
//  RoundImageView.swift
//  Wolfy
//
//  Created by Nessim Halfon on 19/03/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit

class RoundImageView: UIImageView {

    //MARK:- Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    //MARK:- Setup Func
    
    func setup() {
        clipsToBounds = true
        contentMode = .scaleAspectFill
        layer.cornerRadius = frame.height / 2
        layer.borderColor = #colorLiteral(red: 0.841610074, green: 0.8896840215, blue: 0.9313734174, alpha: 1) //#colorLiteral(red: 0.2099020481, green: 0.2616115808, blue: 0.3281416297, alpha: 1)
        layer.borderWidth = 2
    }
}
