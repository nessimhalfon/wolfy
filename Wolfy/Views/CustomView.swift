//
//  CustomView.swift
//  Wolfy
//
//  Created by Nessim Halfon on 19/03/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit

class CustomView: UIView {

 //MARK:- Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
   
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    //MARK:- Setup Func
   
    func setup() {
        layer.cornerRadius = 15
        layer.borderWidth = 2
        layer.borderColor = UIColor.darkGray.cgColor
        backgroundColor = #colorLiteral(red: 0.2099020481, green: 0.2616115808, blue: 0.3281416297, alpha: 1)
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: -4, height: 4)
        layer.shadowRadius = 4
        layer.shadowOpacity = 0.65
    }
    

}
