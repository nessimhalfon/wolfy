//
//  YoutubeAPIService.swift
//  Wolfy
//
//  Created by Nessim Halfon on 25/03/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit

struct YoutubeAPIService {
    
    //MARK:- Properties
    let base = "https://www.googleapis.com/youtube/v3/search"
    let channelsId = "UCuP2vJ6kRutQBfRmdcI92mA"
//    let channelsId = "UCCVQ8CmUbsDOnkaYbnRXRdA"
    
    //MARK:- Func
    
    /// GET https://www.googleapis.com/youtube/v3/search
    func getVideos(_ completion: (([Video]) -> Void)?) {
        if var urlComponents = URLComponents(string: base) {
            urlComponents.queryItems = [
                URLQueryItem(name: "part", value: "snippet"),
                URLQueryItem(name: "channelId", value: channelsId),
                URLQueryItem(name: "maxResults", value: "50"),
                URLQueryItem(name: "type", value: "video"),
                // API_KEY = votre API KEY
                URLQueryItem(name: "key", value: API_KEY)
            ]
            
            guard let url = urlComponents.url else {
                completion?([])
                return
            }
  
            let session = URLSession.shared.dataTask(with: url) { (data, response, error) in
                
                guard let data = data else {
                    completion?([])
                    return
                }
                
                do {
                    let result = try JSONDecoder().decode(APIResult.self, from: data)
                    completion?(result.items)
                } catch {
                    completion?([])
                }
            }
            session.resume()
        }
    }
    
}
