//
//  FirebaseService.swift
//  Wolfy
//
//  Created by Nessim Halfon on 25/03/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage

class FirebaseService {
    
    //MARK:- Properties
    
    var result: ((_ bool: Bool?, _ error: Error?) -> Void)?
    var userValues: [String:String] = [:]
    
    private var database = Database.database().reference()
    private var databaseUser: DatabaseReference {
        return database.child("users")
    }
    private var databaseQuestion: DatabaseReference {
        return database.child("questions")
    }
    
    private let storage = Storage.storage().reference()
    private var storageUser: StorageReference {
        return storage.child("users")
    }
    private var storageAnswer: StorageReference {
        return storage.child("answers")
    }
    
    //MARK:- Auth
    
    func login() -> String? {
        return Auth.auth().currentUser?.uid
    }
    
    func create(_ email: String, password: String, username: String, result: ((_ bool: Bool?, _ error: Error?) -> Void)?) {
        self.result = result
        self.userValues = ["username":username]
        Auth.auth().createUser(withEmail: email, password: password, completion: completion)
    }
    
    func signIn(_ email: String, password: String, result: ((_ bool: Bool?, _ error: Error?) -> Void)?) {
        self.result = result
        Auth.auth().signIn(withEmail: email, password: password, completion: completion)
    }
    
    func signOut() {
        do {
            try Auth.auth().signOut()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func completion(_ restul: AuthDataResult?, _ error: Error?) {
        if let error = error {
            print(error.localizedDescription)
            self.result?(false, error)
        }
        
        if let id = restul?.user.uid {
            updateUser(id, dict: userValues)
            self.result?(true, nil)
        }
    }
    
    //MARK:- DataBase
    
    func updateUser(_ id: String, dict: [String:String]) {
        databaseUser.child(id).updateChildValues(dict)
        userValues = [:]
    }
    
    func fetchUser(_ id: String, _ completion: ((User?) -> Void)?) {
        databaseUser.child(id).observe(.value) { snapshot in
            completion?(User(snapshot: snapshot))
        }
    }
    
    func usernameExist(_ username: String?, _ completion: ((Bool, String?) -> Void)?) {
        guard let username = username, username.count > 2 else {
            completion?(false, "Username trop court")
            return
        }

        databaseUser.queryOrdered(byChild: "username").queryEqual(toValue: username).observeSingleEvent(of: .value) { snapshot in
            if snapshot.exists() {
                completion?(false, "Username déjà pris.")
            } else {
                completion?(true, "")
            }
        }
    }
    
    
    func fetchQuestion(_ completion: ((Question) -> Void)?) {
        databaseQuestion.observe(.childAdded) { snapshot in
            guard let dict = snapshot.value as? [String: Any],
                let date = dict["date"] as? String,
                let userId = dict["userId"] as? String,
                let question = dict["question"] as? String else { return }
            
            let new = Question(ref: snapshot.ref, id: snapshot.key, date: date, userId: userId, question: question)
            completion?(new)
        }
    }
    
    func uploadQuestion(_ question: String) {
        guard let id = login() else { return }
        let dict = [
            "userId":id,
            "question":question,
            "date":Date().toString()
        ]
        databaseQuestion.childByAutoId().updateChildValues(dict)
    }
    
    
    func fetchAnswer(ref: DatabaseReference, _ completion: ((Answer ) -> Void)?) {
        ref.child("answers").observe(.childAdded) { snapshot in
            guard let dict = snapshot.value as? [String:Any],
                let userId = dict["userId"] as? String else { return }
            let answer = Answer(userId: userId, snapshot: snapshot)
            completion?(answer)
        }
    }
    
    func uploadAnswer(ref: DatabaseReference, answer: String?, image: String?,  height: CGFloat?) {
        guard let id = login() else { return }
        var dict: [String:Any] = [
            "userId":id,
            "date":Date().toString()
        ]
        if answer != nil {
            dict["answer"] = answer!
        }
        if image != nil {
            dict["imageUrl"] = image!
        }
        if height != nil {
            dict["imageHeight"] = height!
        }
        ref.child("answers").childByAutoId().updateChildValues(dict) 
    }
    
    //MARK:- Storage
    
    func uploadProfilPicture(_ data: Data) {
        guard let id = login() else { return }
        let reference = storageUser.child(id)
        reference.putData(data, metadata: nil) { (meta, error) in
            if error == nil {
                reference.downloadURL { (url, error) in
                    if let urlString = url?.absoluteString {
                        self.updateUser(id, dict: ["imageUrl":urlString])
                    }
                }
            }
        }
    }
    
    func uploadImageAnswer(_ data: Data, _ completion: ((String?) -> Void)?) {
        let uniqueId = UUID().uuidString
        let reference = storageAnswer.child(uniqueId)
        reference.putData(data, metadata: nil) { (meta, error) in
            if error != nil {
                completion?(nil)
            } else {
                reference.downloadURL { (url, error) in
                    if error == nil, let urlString = url?.absoluteString {
                        completion?(urlString)
                    } else {
                        completion?(nil)
                    }
                }
            }
        }
    }
    
}
