//
//  RSSParser.swift
//  Wolfy
//
//  Created by Nessim Halfon on 19/03/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import Foundation

class RSSParser: NSObject {
    
    //MARK:- Properties
    var articles: [Article] = []
    
    var title = ""
    var pubDate = ""
    var link = ""
    var imageUrl = ""
    
    var element = ""
    
    var completion: (([Article]) -> Void)?
    
    //MARK:- Func
    
    func parse(_ urlString: String, completion: (([Article]) -> Void)?) {
        self.completion = completion
        guard let url = URL(string: urlString) else {
            self.completion?(articles)
            return
        }
        let session = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard error == nil, let data = data else {
                self.completion?(self.articles)
                return
            }
            
            let parser = XMLParser(data: data)
            parser.delegate = self
            parser.parse()
        }
        session.resume()
    }
}

//MARK:-
extension RSSParser: XMLParserDelegate {
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        
        element = elementName
        
        if element == "item" {
            title = ""
            pubDate = ""
            link = ""
            imageUrl = ""
        }
        
        if element == "media:content" {
            if let url = attributeDict["url"] {
                imageUrl = url
            }
        }
        
        
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        switch element {
        case "title": title += string
        case "link":
            if link == "" {
                link = string
            }
        case "pubDate":
            if pubDate == "" {
                pubDate = string
            }
        default: break
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "item" {
            let new = Article(title: title, link: link, pubDate: pubDate, imageUrl: imageUrl)
            articles.append(new)
        }
    }
    
    func parserDidEndDocument(_ parser: XMLParser) {
        self.completion?(articles)
    }
    
    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        self.completion?(articles)
    }
    
}
